package destroy_eni

import (
	"encoding/json"
)

type Response struct {
}

func newResponseStruct() *Response {
	return &Response{}
}

func NewResponse() map[string]interface{} {
	in := newResponseStruct()

	var inInterface map[string]interface{}
	inrec, _ := json.Marshal(in)
	json.Unmarshal(inrec, &inInterface)
	return inInterface
}
