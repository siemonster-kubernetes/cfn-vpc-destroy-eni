package destroy_eni

type ResourceProperties struct {
	ServiceToken   string   `json:"ServiceToken"`
	SecurityGroups []string `json:"SecurityGroups"`
}
