package destroy_eni

import (
	"context"
	"encoding/json"
	"github.com/aws/aws-lambda-go/cfn"
	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/service/ec2"
	"github.com/aws/aws-sdk-go-v2/service/ec2/types"
	"gitlab.com/siemonster-kubernetes/cfn-vpc-destroy-eni/app/custom"
	"time"
)

type DestroyEni struct {
	PhysicalResourceID string
	Data               map[string]interface{}
	properties         ResourceProperties

	event  cfn.Event
	config aws.Config
}

func getPhysicalResourceID(event cfn.Event) string {
	if event.PhysicalResourceID != "" {
		return event.PhysicalResourceID
	}
	return custom.NewPhysicalResourceID(event)
}

func NewDestroyEni(ctx context.Context, event cfn.Event, config aws.Config) *DestroyEni {
	jsonbody, err := json.Marshal(event.ResourceProperties)
	if err != nil {
		panic(err)
	}
	properties := ResourceProperties{}
	if err := json.Unmarshal(jsonbody, &properties); err != nil {
		panic(err)
	}

	return &DestroyEni{
		PhysicalResourceID: getPhysicalResourceID(event),
		event:              event,
		config:             config,
		properties:         properties,
	}
}

func (x DestroyEni) Create() (physicalResourceID string, data map[string]interface{}, err error) {
	return x.PhysicalResourceID, x.Data, nil
}

func (x DestroyEni) Update() (physicalResourceID string, data map[string]interface{}, err error) {
	client := ec2.NewFromConfig(x.config)
	params := &ec2.DescribeNetworkInterfacesInput{
		Filters: []types.Filter{
			{
				Name:   aws.String("group-id"),
				Values: x.properties.SecurityGroups,
			},
			{
				Name: aws.String("description"),
				Values: []string{
					"AWS Lambda VPC ENI: *",
				},
			},
		},
	}
	resp, err := client.DescribeNetworkInterfaces(context.TODO(), params)
	if err != nil {
		return x.PhysicalResourceID, x.Data, err
	}

	interfaceIds := make([]string, len(resp.NetworkInterfaces))
	attachmentIds := make([]string, len(resp.NetworkInterfaces))

	for i, networkInterface := range resp.NetworkInterfaces {
		interfaceIds[i] = *networkInterface.NetworkInterfaceId
		attachmentIds[i] = *networkInterface.Attachment.AttachmentId
	}

	waitInput := &ec2.DescribeNetworkInterfacesInput{
		NetworkInterfaceIds: interfaceIds,
	}
	waiter := ec2.NewNetworkInterfaceAvailableWaiter(client)
	maxWaitTime := 5 * time.Minute
	err = waiter.Wait(context.TODO(), waitInput, maxWaitTime)
	if err != nil {
		return x.PhysicalResourceID, x.Data, err
	}

	deleteInput := &ec2.DeleteNetworkInterfaceInput{
		NetworkInterfaceId: &interfaceIds[0],
	}
	_, err = client.DeleteNetworkInterface(context.TODO(), deleteInput)
	if err != nil {
		return x.PhysicalResourceID, x.Data, err
	}

	return x.PhysicalResourceID, NewResponse(), err
}

func (x DestroyEni) Delete() (physicalResourceID string, data map[string]interface{}, err error) {
	return x.PhysicalResourceID, x.Data, nil
}

func (x DestroyEni) Handle() (physicalResourceID string, data map[string]interface{}, err error) {
	switch x.event.RequestType {
	case cfn.RequestCreate:
		return x.Create()
	case cfn.RequestUpdate:
		return x.Update()
	case cfn.RequestDelete:
		return x.Delete()
	}

	return
}
