package main

import (
	"context"
	"fmt"
	"log"

	"gitlab.com/siemonster-kubernetes/cfn-vpc-destroy-eni/app/custom/destroy_eni"

	"github.com/aws/aws-lambda-go/cfn"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go-v2/config"
)

func handleCustomResources(ctx context.Context, event cfn.Event) (physicalID string, data map[string]interface{}, err error) {
	cfg, err := config.LoadDefaultConfig(context.TODO())
	if err != nil {
		log.Fatalf("unable to load SDK config, %v", err)
	}

	fmt.Println(event)
	switch event.ResourceType {
	case "Custom::DestroyENI":
		custom := destroy_eni.NewDestroyEni(ctx, event, cfg)
		resourceId, data, err := custom.Handle()
		fmt.Println(data)
		return resourceId, data, err
	default:
		return "", nil, fmt.Errorf("unknown resource type %s", event.ResourceType)
	}
}

func main() {
	lambda.Start(cfn.LambdaWrap(handleCustomResources))
}
