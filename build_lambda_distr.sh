#!/usr/bin/env bash
set -e

BUILD_CONTAINER_IMAGE=cfn-vpc-destroy-eni
ARCHIVE_FILE=lambda.zip

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd "$DIR"

rm -rf "${DIR}/lambda/${ARCHIVE_FILE}"

docker build -t "${BUILD_CONTAINER_IMAGE}" -f .docker/Dockerfile .docker
docker run --rm -i -v "${DIR}:/src" -w "/src" --user=root --entrypoint /src/.docker/build.sh "${BUILD_CONTAINER_IMAGE}"

rm -rf "${DIR}/build"
rm -rf "${DIR}/bin"
rm -rf "${DIR}/.cache"
